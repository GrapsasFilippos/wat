#!/bin/bash

echo "Run \`source path_exporter\` to set the \$PATH to the current console."

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export PATH=$DIR/node/bin:$DIR/node_modules/.bin/:$PATH

echo node -v
node -v
echo npm -v
npm -v
