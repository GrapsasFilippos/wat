# WAt - Web at

Η εντολή at για το web. Υπηρεσία με βοήθεια της οποίας γίνεται δυνατή η εκτέλεση εργασιών σε καθορισμένο (κατά βάση μελλοντικό) χρόνο. Συγκεκριμένα:

* Δέχεται αιτήματα HTTP για τον χρονομπρογραμματισμό εργασιών και όταν παρέλθει αυτός ο χρόνος στέλνει αίτημα HTTP ώστε να ειδοποιήσει κάποια άλλη διαδικτυακή υπηρεσία και αυτή με την σειρά της να εκτελέσει την εργασία της.
* Κατά την εκκίνησή του το WAt, επικοινωνεί με τις δηλωμένες υπηρεσίες ώστε να λάβει τυχών εργασίες που προϋπάρχουν.


## Documentation

### Installation
1. [Set up ssh key for Git](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)
2. `cd /path/to/project/parent/dir/`
3. `git clone git@bitbucket.org:dgagr/wat.git`
4. `cd wat/`
5. `git checkout develop`
6. `cp config.yml.example config.yml`
7. Fill `config.yml`
8. `make install`
9. `make dev-run`
