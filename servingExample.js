const http = require( 'http' );
const express = require( 'express' );

const misc = require( './misc' );


var log = misc.getWinston( 'ServingExampleLogger', 'Example' );


function server( config ) {
  const app = express();

  app.use( function( req, res, next ) {
    log.info( 'Request ' + req.path );
    next();
  } );

  app.get( '/', function( req, res ) {
    res.set( 'Content-Type', 'text/plain' );
    res.send( 'servingExample' );
  } );

  app.get( '/list', function( req, res ) {
    res.set( 'Content-Type', 'text/plain' );
    res.send( JSON.stringify( [
      {
        host: 'localhost',
        port: config.servingExample.port,
        path: '/command1',
        time: misc.getTimestamp() + 2,
      },
      {
        host: 'localhost',
        port: config.servingExample.port,
        path: '/command2',
        time: misc.getTimestamp() + 6,
      },
    ], null, 4 ) );
  } );

  const server = app.listen( config.servingExample.port, function () {
    log.info( 'Server running at http://127.0.0.1:' +
      config.servingExample.port + '/' );
  } );
}

function addOnDemand( config ) {
  var req = http.request( {
      method: 'POST',
      host: 'localhost',
      port: config.port,
      path: '/At',
      headers: {
        'content-type': 'application/json',
      },
  } );
  req.write( JSON.stringify( {
      host: 'localhost',
      port: config.servingExample.port,
      path: '/onDemandCommand1',
      time: misc.getTimestamp() + 2,
    } ) );
  req.end();
}

function begin( config ) {
  server( config );
  setTimeout( function() {
    addOnDemand( config );
  }, 2000 );
}


exports.server = server;
exports.begin = begin;
