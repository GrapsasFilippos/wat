SHELL=/bin/bash
UNAME := $(shell uname)
DIR := ${CURDIR}
export PATH := $(DIR)/node/bin:$(DIR)/node_modules/.bin/:$(PATH)


SCRIPT_PATH := $(DIR)/src/main.js
SCRIPT_CONFIG_PATH := $(DIR)/config.yml


deploy: node/bin/npm
		npm install --production

deploy-dev: node/bin/npm
		npm install


include Makefile.nodejs.mk


dev-run:
		npm -v
		node -v
		DEBUG_LVL=silly node $(SCRIPT_PATH) $(SCRIPT_CONFIG_PATH)

dev-run-pm2:
		npm -v
		node -v
		pm2 -v
		-pm2 delete WAt
		DEBUG_LVL=silly pm2 --no-daemon --name WAt start $(SCRIPT_PATH) -- $(SCRIPT_CONFIG_PATH)


# node_modules/.bin/gulp: install-node-dependencies
#
# install-node-dependencies: node/bin/npm
# 		npm install
