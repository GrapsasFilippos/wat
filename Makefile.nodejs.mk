install-node: node/bin/npm

uninstall-node:
		-yes | rm -Ir ./node/


node/bin/npm:
ifeq ($(UNAME),Linux)
		wget https://nodejs.org/dist/v6.11.0/node-v6.11.0-linux-x64.tar.gz
		tar -xf node-v6.*-linux-x64.tar.gz
		mv node-v6*/ node/
		rm node-v6*.tar.gz
endif
ifeq ($(UNAME),Darwin)
		curl https://nodejs.org/dist/v6.11.0/node-v6.11.0-darwin-x64.tar.gz
		tar -xf node-v6.tar.gz
		mv node-v6*/ node/
		rm node-v6.tar.gz
endif
