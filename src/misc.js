const winston = require( 'winston' );


function getTimestamp() {
  var d = new Date();
  return d.getTime() / 1000 | 0;
}

function getWinston( name, label ) {
  winston.loggers.add( name, {
    console: {
      level: winston.level,
      colorize: true,
      label: label,
      timestamp: true,
    },
  } );
  return winston.loggers.get( name );
}


exports.getTimestamp = getTimestamp;
exports.getWinston = getWinston;



winston.level = process.env.DEBUG_LVL ?
  process.env.DEBUG_LVL :
  'info';
winston.silly( 'Environment variable DEBUG_LEVEL: ' + process.env.DEBUG_LVL );
