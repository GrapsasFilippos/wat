/**
 * @author <Filippos A. Grapsas> fgrapsas@dga.gr
 *
 * Created at 13/05/2019
 */
'use strict';

const http = require('http');
const misc = require('./misc');

const log = misc.getWinston('IntervalsLogger', 'Intervals');

module.exports = class Intervals {
  /**
   * constructor
   * @param {object} config
   */
  constructor(config) {
    this.className = 'Intervals';
    const methName = `${this.className}.constructor`;
    log.debug(`${methName}()`);

    this.config = config;
  }

  /**
   * start
   */
  start() {
    const methName = `${this.className}.start`;
    log.debug(`${methName}()`);

    if (this.config &&
        this.config.intervals &&
        Array.isArray(this.config.intervals)
    ) {
      this.intervals = this.config.intervals.concat();

      for (let i = 0; i < this.intervals.length; i++) {
        const item = this.intervals[i];
        const requestOptions = {
          method: 'GET',
          host: item.host,
          port: item.port,
          path: item.path,
        };

        item.stoper = setInterval(() => {
          const randNum = Math.floor(Math.random() * (99999 - 10000) + 10000);
          log.info(`${methName}() - exec`, randNum, requestOptions);
          http.request(requestOptions)
            .on('response', function(response) {
              const lvl = (response.statusCode === 200) ? 'info' : 'warn';
              log.log(
                lvl,
                `${methName}() - resp`,
                randNum,
                `Http Response Status code: ${response.statusCode}`
              );
            })
            .on('error', function(response) {
              log.warn(`${methName}() - resp`, randNum, `${response.code}`);
            })
            .end()
          ;
        }, item.interval * 1000);
      }
    }
  }
};
