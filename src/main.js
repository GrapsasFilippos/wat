const fs = require( 'fs' );
const yaml = require( 'js-yaml' );
const express = require( 'express' );
const bodyParser = require( 'body-parser' );

const ctrl = require( './Controllers/WAtController' );
const at = require( './At.js' );
const misc = require( './misc.js' );
const Intervals = require( './Intervals' );


let log = misc.getWinston( 'WAtLogger', 'WAt' );


// config
let config;
try {
  config = yaml.safeLoad( fs.readFileSync( process.argv[2], 'utf8' ) );
  log.silly( 'config ' + JSON.stringify( config ) );
} catch ( e ) {
  log.error( e );
  process.exit( 1 );
}


// servingExample
if ( config.servingExample.enabled ) {
  const servingExample = require( './servingExample' );
  servingExample.begin( config );
}
if ( config.sentryDSN ) {
  const Raven = require( 'raven' );
  Raven.config( config.sentryDSN ).install();
}


// main app
const app = express();

app.use( ctrl.middleware );
app.use( bodyParser.json() );
app.get( '/', ctrl.home );
app.post( '/At', ctrl.addAt );

const server = app.listen( config.port, function() {
  log.info( 'Server running at http://127.0.0.1:' + config.port + '/' );
} );


at.load( config );

const intervals = new Intervals(config);
intervals.start();
