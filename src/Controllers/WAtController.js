const at = require( './../At.js' );
const misc = require( './../misc.js' );


const log = misc.getWinston( 'WAtControllerLogger', 'WAtController' );


function middleware( request, response, next ) {
  log.info( 'Request ' + request.path );
  next();
}

function home( request, response ) {
  response.set( 'Content-Type', 'text/plain' );
  response.send( 'WAt' );
  response.end();
}

function addAt( request, response ) {
  const atCommand = request.body;
  log.silly( 'Request body: ' + JSON.stringify( atCommand ) );
  at.add( atCommand );
  response.end();
}


exports.middleware = middleware;
exports.home = home;
exports.addAt = addAt;
