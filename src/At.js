const http = require( 'http' );
const winston = require( 'winston' );

const misc = require( './misc' );


var log = misc.getWinston( 'AtLogger', 'At' );

var atList = [];


/**
 * [atExec description]
 * @param  {[type]} at [description]
 */
function atExec( at ) {
  http.request(
    {
      method: 'GET',
      host: at.host,
      port: at.port,
      path: at.path,
    },
    function( response ) {
      var str = '';
      response
        .on( 'data', function( chunk ) {
          str += chunk;
        } )
        .on( 'end', function() {
        } );
    }
  ).end();
}

/**
 * [add description]
 * @param {[type]} at [description]
 */
function add( at ) {
  let time = at.time - misc.getTimestamp();
  time = time > 0 ? time : 0;
  at.timeout = setTimeout( function() {
    log.info( 'Timeout ' + at.host + ':' + at.port + at.path );
    atExec( at );
    remove( at );
  }, time * 1000 );
  atList.push( at );
}

/**
 * [removeByIndex description]
 * @param  {[type]} index [description]
 */
function removeByIndex( index ) {
  clearTimeout( atList[ index ] );
  atList.splice( index, 1 );
}

/**
 * [remove description]
 * @param  {[type]} at [description]
 */
function remove( at ) {
  removeByIndex( atList.indexOf( at ) );
}

/**
 * [load description]
 * @param  {[type]} config [description]
 */
function load( config ) {
  for( i in config.atList ) {
    var req = http.request(
      {
        method: 'GET',
        host: config.atList[ i ].host,
        path: config.atList[ i ].path,
        port: config.atList[ i ].port
      },
      function( response ) {
        var str = '';
        response
          .on( 'data', function( chunk ) {
            str += chunk;
          } )
          .on( 'end', function() {
            var at = JSON.parse( str );
            for( j in at ) {
              add( at[ j ] );
            }
          } );
      }
    ).end();
  }
}

/**
 * [getAtList description]
 * @return {[type]} [description]
 */
function getAtList() {
  return atList;
}

exports.add = add;
exports.load = load;
exports.getAtList = getAtList;
